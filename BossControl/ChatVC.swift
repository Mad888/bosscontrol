//
//  ChatVC.swift
//  BossControl
//
//  Created by Macmini Admin on 1/20/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Firebase

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}





class ChatVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var labelChat: UIImageView!
    @IBOutlet weak var headLabelChat: UIImageView!
    @IBOutlet weak var sendMessageLabel: UIImageView!
    @IBOutlet weak var chatContainer: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textFieldChat: UITextField!
    @IBOutlet weak var textToSend: UIImageView!
    
    
    @IBOutlet weak var imageBackground: UIImageView!
    
    var userFromId = ""
    var userToId  = ""
    var messages = [Message] ()
    var messagesDictionary = [String: Message] ()
    
    var url = String ()
    var image = UIImage ()
    
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        setup()
//        observeMessages()
//        
//    }
//    
    
    
//    func setup () {
//        labelChat.layer.cornerRadius = 19
//        imageBackground.layer.cornerRadius = 15
//        chatContainer.delegate = self
//        chatContainer.dataSource = self
//        getUserId()
//        userToId = UserDefaults.standard.object(forKey: "toId") as! String
//        textFieldChat.delegate = self
//        textFieldChat.attributedPlaceholder = NSAttributedString (string:"Enter text...", attributes:[.foregroundColor: UIColor.lightGray,.font: UIFont.init(name:"Futura", size: 15)!])
//
//    }
//
    
    // Get useruid
    func getUserId () {
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                if let index = user?.uid {
                    self.userFromId = index
                   
                }
            }else {
                print ("NO REGISTRATION")
                
            }
        }
    }
    
    
    
    @IBAction func sendButton(_ sender: Any) {
        
        let ref = Database.database().reference().child("Chat")
        let childRef = ref.childByAutoId()
        let timestamp = Int(NSDate().timeIntervalSince1970)
        let values = ["text": self.textFieldChat.text!, "toId": self.userToId,  "fromId": self.userFromId,"timestamp": timestamp] as [String : Any]
        childRef.updateChildValues(values)
        
    }
    
    
    
    
    // Download User Information for Collection View
    func observeMessages () {
        let ref = Database.database().reference().child("Chat")
        ref.observe(.childAdded, with: { (snapshot) in
            
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let message = Message(dictionary: dictionary)
               self.messages.append(message)
                
                if let toId = message.toId {
                    self.messagesDictionary[toId] = message

                    self.messages = Array(self.messagesDictionary.values)
                    self.messages.sort(by: { (message1, message2) -> Bool in
                        return message1.timestamp?.int32Value > message2.timestamp?.int32Value
                    })
                }
                
                DispatchQueue.main.async {
                    self.chatContainer.reloadData()
                }
                
            }
        }, withCancel: nil)
        
        
    }
    
    

        
    }
    
    
    
    
    
    


extension ChatVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
           let cell = chatContainer.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! ChatCell
     
        let message = messages[indexPath.row]
        
        if let toId = message.toId {
            let ref = Database.database().reference().child("Worker").child(toId)
            ref.observe(.value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    cell.fromUserMessage.text = dictionary["username"] as? String
                    self.url = (dictionary["fotoImageUrl"] as? String)!
                }
                
                    let pathReference = Storage.storage().reference(forURL: self.url)
                    pathReference.getData(maxSize: 10 * 1024 * 1024) { data, error in
                        if error == nil {
                            self.image = UIImage(data: data!)!
                            print ("OOOOOOOK -\(self.image.description)")
                        } else {
                            print ("error")
                        }
                        
                        DispatchQueue.main.async {
                            cell.imageProfileChat.image  = self.image
                        }
                    }
            }, withCancel: nil)
            
        }
        
        cell.textMessage.text = message.text
        
        let myDate =  message.timestamp?.doubleValue
        let timestamp = Date(timeIntervalSince1970: myDate!)
        let format = DateFormatter()
        format.dateFormat = "hh:mm:ss a"
        cell.timeStampLabel.text = format.string(from: timestamp)
        return cell
    }
    
    

    
  
    
    
    
}


