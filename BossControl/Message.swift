//
//  Message.swift
//  BossControl
//
//  Created by Macmini Admin on 1/20/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
class Message: NSObject {
    
    var fromId: String?
    var text: String?
    var toId: String?
    var timestamp: NSNumber?
    
    init (dictionary: [String: Any]) {
        self.fromId = dictionary ["fromId"] as? String
        self.text = dictionary ["text"] as? String
        self.toId = dictionary ["toId"] as? String
        self.timestamp = dictionary ["timestamp"] as? NSNumber
    }
    
}


