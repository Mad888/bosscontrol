//
//  StartVC.swift
//  BossControl
//
//  Created by Macmini Admin on 1/9/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Firebase

class StartVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var labelMailStartVC: UITextField!
    @IBOutlet weak var passwordStartVC: UITextField!
    @IBOutlet weak var startButton: ShakeAnimation!
    @IBOutlet weak var logoCompany: UIImageView!
    
    var emailBoss = String()
    var emailBosse = [String] ()
    var emailWorker = String()
    var emailWorkers = [String] ()
     var userUid = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
       
        setup()
       signOut()
        
    }
    
    func setup () {
//        logoCompany.isHidden = true
        logoCompany.layer.cornerRadius = 20
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            self.logoCompany.isHidden = false
            self.logoCompany.fadeIn()
        }
        
        self.labelMailStartVC.delegate = self
        self.passwordStartVC.delegate = self
        labelMailStartVC .attributedPlaceholder = NSAttributedString (string:"Enter email", attributes:[.foregroundColor: UIColor.lightGray,.font: UIFont.init(name:"Futura", size: 14)!])
        passwordStartVC.attributedPlaceholder = NSAttributedString (string:"Enter password", attributes:[.foregroundColor: UIColor.lightGray,.font: UIFont.init(name:"Futura", size: 14)!])
        startButton.layer.cornerRadius = 12
        bringBoss()
        bringWorker()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            print (self.emailBosse)
            self.defineBossAndWorkers()
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true }
    
    
    
    // Load info from Firebase to define Bosses
    func bringBoss() {
        Database.database().reference().child("Boss").observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            self.emailBoss = value? ["usermail"] as? String ?? ""
            self.emailBosse.append(self.emailBoss)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    func bringWorker() {
        Database.database().reference().child("Worker").observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            self.emailWorker = value? ["usermail"] as? String ?? ""
            self.emailWorkers.append(self.emailWorker)
           
        
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    // Define Bosses
    func defineBossAndWorkers () {
        Auth.auth().addStateDidChangeListener() { auth, user in
            if let index = user?.email {
                if self.emailBosse.contains(index) {
                    print ("Это босс")
                    self.performSegue(withIdentifier: "BossSegue", sender: self)
                } else if self.emailWorkers.contains(index) {
                    print ("Это работник")
                    if let indexuid = user?.uid{
                        self.userUid = indexuid
                 
                        
                        UserDefaults.standard.set(self.userUid, forKey: "userId")
                    }
                    
                    self.performSegue(withIdentifier: "WorkerSegue", sender: self)
                }
        
            }
             else   {
                print ("нет регистрации")
            }
        }
    }
    
    
    
    
    
    
    
    
    // Make Log Out
    func signOut () {
        do {
            try Auth.auth().signOut()
        }
        catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    
    @IBAction func buttonSignIn(_ sender: ShakeAnimation) {
       sender.shake ()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            Auth.auth().signIn(withEmail: self.labelMailStartVC.text!, password: self.passwordStartVC.text!) { (user, error) in
            if error == nil{
               
            }
            else{
                self.startButton.backgroundColor = .red
                let alertController = UIAlertController(title: "Увага!", message: "Заполните поля", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "Попробуй еще раз", style: .destructive  , handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
            self.defineBossAndWorkers ()
    }
    
    }
    
    
    
}
extension UIView {
    
    func fadeIn(_ duration: TimeInterval = 5.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 1.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.3
        }, completion: completion)
    }
}
