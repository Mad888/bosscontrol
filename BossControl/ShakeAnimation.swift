//
//  ShakeAnimation.swift
//  BossControl
//
//  Created by Macmini Admin on 1/20/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
class ShakeAnimation: UIButton {
    func shake () {
        let shakeAnimation = CABasicAnimation.init(keyPath: "position")
        shakeAnimation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        shakeAnimation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        shakeAnimation.duration = 0.04
        shakeAnimation.repeatCount = 10
        shakeAnimation.autoreverses = true
        layer.add(shakeAnimation, forKey: "position")
        
    }
}

