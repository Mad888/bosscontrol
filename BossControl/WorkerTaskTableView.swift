//
//  WorkerTaskTableView.swift
//  BossControl
//
//  Created by Macmini Admin on 1/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Firebase
import MapKit
import CoreMotion

class WorkerTaskTableView: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var workersTaskTableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    
    var userUid = String ()
    var userTasks = [String] ()
    var userComment = [String] ()
    // Map properies
    let locationManager = CLLocationManager()
    var coordinates = [CLLocationCoordinate2D] ()
    let annotation = MKPointAnnotation()
    let annotation2 = MKPointAnnotation()
    var latitude = [CLLocationDegrees] ()
    var longitude = [CLLocationDegrees] ()
    var workerLocation = [CLLocationDegrees] ()
    var latitudeString  = String()
    var longitudeString  = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
           
            if self.latitude.last != nil {
                
                self.workerLocation.append(self.latitude.last!)
                self.workerLocation.append(self.longitude.last!)
                let numLat = NSNumber(value: (self.workerLocation.first)! as Double)
                self.latitudeString = numLat.stringValue
                
                let numLat2 = NSNumber(value: (self.workerLocation.last)! as Double)
                self.longitudeString  = numLat2.stringValue
                
                print (self.longitudeString)
                print (self.latitudeString)
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Turn off your location service",   preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
        
                }
               
            

            
            
        }
        
        
        setup()
        
    }
    
    
    func setup () {
        
        // Make utilities for MapView
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            //  locationManager.allowsBackgroundLocationUpdates = true
        }
        mapView.delegate = self
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.layer.cornerRadius = 50
        mapView.mapType = MKMapType.hybridFlyover
        
        
        workersTaskTableView.delegate = self
        workersTaskTableView.dataSource = self
        userUid =  UserDefaults.standard.object(forKey: "userId") as! String
        
        //   Check for tasks in Firebase
        Database.database().reference().child("WorkerMessages").child(userUid).observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            self.userTasks.append(value? ["task"] as? String ??  "")
            self.userComment.append(value? ["comments"] as? String ??  "")
            
            DispatchQueue.main.async {
                self.workersTaskTableView.reloadData()
            }
            
        }, withCancel: nil)
        
    }
    
    
    
    @IBAction func uploadLocationButton(_ sender: Any) {
        let ref = Database.database().reference (fromURL: "https://bosscontrol-bc826.firebaseio.com/")
        let userRef = ref.child("Worker").ref.child(self.userUid).ref.child("coord")
        let value = ["latitude": latitudeString,  "longitude": longitudeString]
        userRef.updateChildValues(value) { (error, ref) in
            if error != nil {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
        }
    
    
    
    
    
    
    }
    
    
 
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        latitude.append(coord.latitude)
        longitude.append(coord.longitude)
        print ("---------------\(latitude)")
        
    }
    
    
    
    
    
    
    
}



extension WorkerTaskTableView:  UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = workersTaskTableView.dequeueReusableCell(withIdentifier: "cellID2", for: indexPath) as! TaskForWorkersTableViewCell
        
        cell.cellLabel.text = userTasks[indexPath.row]
        return cell
    }
    
}
