//
//  RegistrationVC.swift
//  BossControl
//
//  Created by Macmini Admin on 1/9/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Firebase

class RegistrationVC: UIViewController, UITextFieldDelegate  {
    @IBOutlet weak var FotoImage: UIImageView!
    @IBOutlet weak var profileLabel: UITextField!
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var mailLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!

    @IBOutlet weak var choosePositionLabel: UIImageView!
    @IBOutlet weak var bossButton: UIButton!
    @IBOutlet weak var workerButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var background: UIImageView!
    
var selectBossOrWorker = Bool()
var useruid = ""
var fotoImageUrl = ""
var userRefmy = DatabaseReference ()
var fotoWorker = StorageReference ()
var timer = Timer()
var counter: Int = 0
var total: Int = 15
    
var shapeLayer: CAShapeLayer!
var pulsatingLayer: CAShapeLayer!
    
let percentageLabel: UILabel = {
        let label = UILabel()
        label.text = "UPLOAD"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 25)
        label.textColor = .white
        return label
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: NSNotification.Name(rawValue: "nine"), object: nil)
    }
    
    @objc private func handleEnterForeground() {
        animatePulsatingLayer()
    }
    
    private func createCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 100, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = 20
        layer.fillColor = fillColor.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.position = view.center
        return layer
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    signOut()
    setup()
    addGestToImage()
    
    }
    
    
    func action () {
        setupNotificationObservers()
        
        view.backgroundColor = UIColor.backgroundColor
        
        setupCircleLayers()
        
        animateCircle()
        
        setupPercentageLabel()
        
    }
    
    
    
// Label Settings
func setup () {
    self.nameLabel.delegate = self
    self.mailLabel.delegate  = self
    self.passwordLabel.delegate  = self
    self.profileLabel.delegate  = self
    nameLabel .attributedPlaceholder = NSAttributedString (string:"Enter name", attributes:[.foregroundColor: UIColor.lightGray,.font: UIFont.init(name:"Futura", size: 14)!])
    mailLabel.attributedPlaceholder = NSAttributedString (string:"Enter mail", attributes:[.foregroundColor: UIColor.lightGray,.font: UIFont.init(name:"Futura", size: 14)!])
    passwordLabel.attributedPlaceholder = NSAttributedString (string:"Enter password", attributes:[.foregroundColor: UIColor.lightGray,.font: UIFont.init(name:"Futura", size: 14)!])
    profileLabel.attributedPlaceholder = NSAttributedString (string:"Enter profile", attributes:[.foregroundColor: UIColor.lightGray,.font: UIFont.init(name:"Futura", size: 14)!])
    
    }
    
    
    
   
    
    
// Add Imagepicker
    func addGestToImage() {
        FotoImage.addGestureRecognizer(UITapGestureRecognizer (target: self, action: #selector (handlerselector)))
    }
    

// Dissmis keybord
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
        
    }




    @IBAction func choosePositionBoss(_ sender: Any) {
selectBossOrWorker = true
    }
    @IBAction func choosePositionWorker(_ sender: Any) {
    selectBossOrWorker = false
    }
    

  // Log out
func signOut() {
        do {
            try Auth.auth().signOut()
        }
        catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
// Create user Authentication and save to database
    @IBAction func createUserButton(_ sender: Any) {

        if nameLabel.text!.count > 2 && profileLabel.text!.count > 2 && passwordLabel.text!.count > 5  && mailLabel.text!.count > 2 {
        
        
//        Make animate cicle
        background.image = nil
        choosePositionLabel.isHidden = true
        bossButton.isHidden = true
        workerButton.isHidden = true
        createButton.isHidden = true
        background.backgroundColor = .black
        FotoImage.isHidden = true
        profileLabel.isHidden = true
        nameLabel.isHidden = true
        mailLabel.isHidden = true
        passwordLabel.isHidden = true
        action()
        
        //        Create user Authentication
        Auth.auth().createUser(withEmail: mailLabel.text!, password: passwordLabel.text!){ (user, error) in
            if error != nil {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
      
// Get useruid
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                if let index = user?.uid {
                    self.useruid = index
                    print (self.useruid)
                }
            }else {
                print ("NO REGISTRATION")
                
            }
        }
        
 // Save selected Image to storage and get fotoImageUrl
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//        let data = self.FotoImage.image!.pngData() as NSData?
        
        
        var storage = Storage.storage()
        storage = Storage.storage(url:"gs://bosscontrol-bc826.appspot.com/")
        let storageRef = storage.reference()
        
    if self.selectBossOrWorker == false {
        let imagesRef = storageRef.child("Worker").child(self.useruid)
        self.fotoWorker = imagesRef
    } else {
        let imagesRef = storageRef.child("Boss").child(self.useruid)
        self.fotoWorker = imagesRef
        }
        let data  = self.FotoImage.image!.jpegData(compressionQuality: 0.1)
        let uploadTask = self.fotoWorker.putData(data!, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                print (data?.description)
                print (error!)
                return
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.fotoWorker.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
                self.fotoImageUrl = (url?.absoluteString)!
            }
        }
        }
        
// Add user detail to Firebasedatabase
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
   
        let ref = Database.database().reference (fromURL: "https://bosscontrol-bc826.firebaseio.com/")
        if self.selectBossOrWorker == false {
            let userRef = ref.child("Worker").ref.child(self.useruid)
            self.userRefmy = userRef
                } else{
            let userRef = ref.child("Boss").ref.child(self.useruid)
            self.userRefmy = userRef
                }
            let value = ["username": self.nameLabel.text!, "userpassword": self.passwordLabel.text!,"userpProfile": self.profileLabel.text!,"usermail": self.mailLabel.text!, "fotoImageUrl": self.fotoImageUrl,"userId": self.useruid]
        self.userRefmy.updateChildValues(value) { (error, ref) in
            if error != nil {

                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                return
        }
        }
    self.performSegue(withIdentifier: "AfterRegisterBackToSTartVC", sender: self)
        }
        
        } else {
            
            let alertController = UIAlertController(title: "Error", message: "Заполните все поля", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
   
    }

    private func setupPercentageLabel() {
        view.addSubview(percentageLabel)
        percentageLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        percentageLabel.center = view.center
    }
    
    private func setupCircleLayers() {
        pulsatingLayer = createCircleShapeLayer(strokeColor: .clear, fillColor: UIColor.pulsatingFillColor)
        view.layer.addSublayer(pulsatingLayer)
        animatePulsatingLayer()
        
        let trackLayer = createCircleShapeLayer(strokeColor: .trackStrokeColor, fillColor: .backgroundColor)
        view.layer.addSublayer(trackLayer)
        
        shapeLayer = createCircleShapeLayer(strokeColor: .outlineStrokeColor, fillColor: .clear)
        
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        shapeLayer.strokeEnd = 0
        view.layer.addSublayer(shapeLayer)
    }
    
    private func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.toValue = 1.5
        animation.duration = 1.0
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    
    
    
    fileprivate func animateCircle() {
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        basicAnimation.toValue = 1
        
        basicAnimation.duration = 7
        
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }
    
}





// Settings for Imagepicker
extension RegistrationVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func handlerselector () {
        FotoImage.layer.cornerRadius = 40
        FotoImage.clipsToBounds = true
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editindImage = info[.editedImage] as? UIImage {
            FotoImage.image = editindImage
        } else if let originalImage = info[.originalImage]  as? UIImage{
            FotoImage.image  = originalImage
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print ("cancelled")
        dismiss(animated: true, completion: nil)
    }
}
