//
//  MyWorkerCollectionVC.swift
//  BossControl
//
//  Created by Macmini Admin on 1/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Firebase


class MyWorkerCollectionVC: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var workersCollectionVC: UICollectionView!
    var userStruct = UserInfo()
    var users = [UserInfo]()
    var image = UIImage()
    var urls = [String]()
    var countForTaskCount = [String]()
    var taskCount = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        bringUser()
        setup ()
        
    }
    
    
    func setup () {
        workersCollectionVC.delegate = self
        workersCollectionVC.dataSource = self
        self.workersCollectionVC.register(UINib(nibName:"SellCollectionVC", bundle: nil), forCellWithReuseIdentifier: "CollectionCell")
        
    }
    
    
    // Download User Information for Collection View
    func bringUser() {
        Database.database().reference().child("Worker").observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            self.userStruct.userpProfile =  value? ["userpProfile"] as? String ??  ""
            self.userStruct.username = value? ["username"] as? String ?? ""
            self.userStruct.useremail = value? ["usermail"] as? String ?? ""
            self.userStruct.fotoImageUrl = value? ["fotoImageUrl"] as? String ?? ""
            self.userStruct.userId = value? ["userId"] as? String ?? ""
            self.urls.append(self.userStruct.fotoImageUrl!)
            let item = UserInfo(username:  self.userStruct.username!, useremail: self.userStruct.useremail!, userpProfile: self.userStruct.userpProfile!, fotoImageUrl: self.userStruct.fotoImageUrl!, userId: self.userStruct.userId!)
            self.users.append(item)
            DispatchQueue.main.async {
                self.workersCollectionVC.reloadData()
            }
            
        }, withCancel: nil)
    
    }
            
    
    // For seque to another VC and send information
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVC" {
            if let destVC = segue.destination as?  TasksVC {
                
                // Can send all infomation model UserInfo from cell
                let info = sender as? UserInfo
                destVC.userTaskFirebaseChild = (info?.userId)!
            }
            
        }
        
    }
    
    
    
    
    // Create custom cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = workersCollectionVC.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! SellCollectionVC
        let currentItem = users[indexPath.row]
        cell.nameLabel.text = currentItem.username
        cell.profileLabel.text = currentItem.userpProfile
 
 // Make a number of task exist view
if let taskCount = currentItem.userId {
Database.database().reference().child("WorkerMessages").child(taskCount).observe(.childAdded, with: { (snapshot) in
  
let value = snapshot.value as? NSDictionary
    if Int (snapshot.key) == 0 {
    self.countForTaskCount.removeAll()
    }
self.taskCount.append(value? ["task"] as? String ??  "")
self.countForTaskCount.append(taskCount)
cell.numberTasks.text = String (self.countForTaskCount.count)
  
}, withCancel: nil)
       }
        
        
        if let ptofileUrl = currentItem.fotoImageUrl {
            let pathReference = Storage.storage().reference(forURL: ptofileUrl)
            pathReference.getData(maxSize: 10 * 1024 * 1024) { data, error in
                if error == nil {
                    self.image = UIImage(data: data!)!
                    print ("OOOOOOOK -\(self.image.description)")
                } else {
                    print ("error")
                }
                
                DispatchQueue.main.async {
                    cell.userFoto.image = self.image
                }
            }
        }
        return cell
    }
    
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
        let info = users[indexPath.row]
        self.performSegue(withIdentifier: "showVC", sender: info)
    }
    
}
