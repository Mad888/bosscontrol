//
//  SellCollectionVC.swift
//  BossControl
//
//  Created by Macmini Admin on 1/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SellCollectionVC: UICollectionViewCell {

 
    
    
    
    @IBOutlet weak var statusBarGeneral: UIImageView!
    @IBOutlet weak var statusBarcurrentTask: UIImageView!
    @IBOutlet weak var message: UIImageView!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userFoto: UIImageView!
    @IBOutlet weak var bagroundCell: UIImageView!
    @IBOutlet weak var numberTasks: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bagroundCell.layer.cornerRadius = 20
        statusBarcurrentTask.layer.cornerRadius = 30
        userFoto.layer.cornerRadius = 15
        message.layer.cornerRadius = 13
    
    }

}
