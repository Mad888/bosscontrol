//
//  AddTaskVC.swift
//  BossControl
//
//  Created by Macmini Admin on 1/16/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Firebase

class AddTaskVC: UIViewController {


@IBOutlet weak var newTaskLabel: UITextField!
@IBOutlet weak var detailTaskTextView: UITextView!
    
    var usermailreceiveAddTaskVC = String ()
    var taskCountFirebase = String ()
    var tasksCountFirebase = [String] ()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

  
         setup ()
        

    }
    
    
    
    
    
    
func setup () {
detailTaskTextView.layer.cornerRadius = 25
    
   
// Count tasks in Firebase
Database.database().reference().child("WorkerMessages").child(usermailreceiveAddTaskVC).observe(.childAdded, with: { (snapshot) in
        let value = snapshot.value as? NSDictionary
        self.taskCountFirebase =  value? ["task"] as? String ??  ""
        self.tasksCountFirebase.append(self.taskCountFirebase)
    }, withCancel: nil)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "backToTaskVC" {
            if let destVC = segue.destination as?  TasksVC {
                destVC.userTaskFirebaseChild = usermailreceiveAddTaskVC
            }
            
        }
        
    }
    
    @IBAction func addTaskButton(_ sender: Any) {
        let ref = Database.database().reference (fromURL: "https://bosscontrol-bc826.firebaseio.com/")
            let userRef = ref.child("WorkerMessages").ref.child(usermailreceiveAddTaskVC).ref.child(String(tasksCountFirebase.count))

        let value = ["task": newTaskLabel.text , "comments": detailTaskTextView.text ]
        userRef.updateChildValues(value) { (error, ref) in
            if error != nil {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
        }
        
        self.performSegue(withIdentifier: "backToTaskVC", sender: self)
    }
     
    

}





