//
//  ChatCell.swift
//  BossControl
//
//  Created by Macmini Admin on 1/21/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    
    
    @IBOutlet weak var imageProfileChat: UIImageView!
    
    @IBOutlet weak var fromUserMessage: UILabel!
    
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var textMessage: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        imageProfileChat.layer.cornerRadius = 20
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
