//
//  TaskForWorkersTableViewCell.swift
//  BossControl
//
//  Created by Macmini Admin on 1/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class TaskForWorkersTableViewCell: UITableViewCell {

    @IBOutlet weak var cellLabel: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
