//
//  UserInfo.swift
//  BossControl
//
//  Created by Macmini Admin on 1/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

struct UserInfo{

    var username: String?
    var useremail: String?
    var userpProfile: String?
    var fotoImageUrl: String?
     var userId: String?
}
