//
//  InvitationAnimation.swift
//  BossControl
//
//  Created by Macmini Admin on 1/22/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class InvitationAnimation: UIViewController {

    @IBOutlet weak var KoltK: UIImageView!
    @IBOutlet weak var koltO: UIImageView!
    @IBOutlet weak var KoltL: UIImageView!
    @IBOutlet weak var KoltT: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            
        
        let duration1: Double = 1.4
        UIImageView.animate(withDuration: duration1, animations: {
            self.moveK(view: self.KoltK)
        })
        
        let duration2: Double = 1.4
        UIImageView.animate(withDuration: duration2, animations: {
            self.moveO(view: self.koltO)
        })
        let duration3: Double = 1.4
        UIImageView.animate(withDuration: duration3, animations: {
            self.moveL(view: self.KoltL)
        })
        let duration4: Double = 1.4
        UIImageView.animate(withDuration: duration4, animations: {
            self.moveT(view: self.KoltT)
        })
        
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
           self.performSegue(withIdentifier: "ShowStartVC", sender: self)

        }
        
        }
        
      

    func moveK (view: UIView) {
        view.center.x -= 200
      self.KoltK.transform = CGAffineTransform (rotationAngle: CGFloat(90))
        }
    func moveO (view: UIView) {
        view.center.y -= 500
     self.koltO.transform = CGAffineTransform (rotationAngle: CGFloat(90))
    }
    func moveL (view: UIView) {
        view.center.y += 500
     self.KoltL.transform = CGAffineTransform (rotationAngle: CGFloat(90))
    
    }
    func moveT (view: UIView) {
        view.center.x += 200
   self.KoltT.transform = CGAffineTransform (rotationAngle: CGFloat(90))
    
    }
    
    
    
      
    
    
    
    
    
    
    

}
