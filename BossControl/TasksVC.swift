//
//  TasksVC.swift
//  BossControl
//
//  Created by Macmini Admin on 1/16/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Firebase
import MapKit
import CoreMotion

class TasksVC: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    @IBOutlet weak var tasksTableView: UITableView!
    @IBOutlet weak var goToChat: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    var userTasks = [String]()
    var userComment = [String]()
    var userTaskFirebaseChild = String ()
    
    
    
    // For LocationManager
    let locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    var coordinatesWorker =  [String] ()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()   
        setup()
        
    }
    
    
    
    
    func setup () {
        goToChat.layer.cornerRadius = 10
          UserDefaults.standard.set(userTaskFirebaseChild, forKey: "toId")
        
        // Make utilities for MapView
        
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            //            locationManager.allowsBackgroundLocationUpdates = true
        }
        mapView.delegate = self
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.layer.cornerRadius = 50
        mapView.mapType = MKMapType.hybridFlyover
        
        tasksTableView.delegate = self
        tasksTableView.dataSource = self
        
        // Remove all punctuation for make a child for Firebase
        
        tasksTableView.layer.cornerRadius = 20
        
       
        //   Check for tasks in Firebase
        Database.database().reference().child("WorkerMessages").child(userTaskFirebaseChild).observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            self.userTasks.append(value? ["task"] as? String ??  "")
            self.userComment.append(value? ["comments"] as? String ??  "")
            DispatchQueue.main.async {
                self.tasksTableView.reloadData()
            }
            
        }, withCancel: nil)
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToTaskVC" {
            if let destVC = segue.destination as?  AddTaskVC {
                destVC.usermailreceiveAddTaskVC = userTaskFirebaseChild
            }
            
        }
        
    }
    
    
    @IBAction func addTaskButton(_ sender: Any) {
        self.performSegue(withIdentifier: "goToTaskVC", sender: self)
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
       

        
    }
    
    
    @IBAction func uploadLocationButton(_ sender: Any) {
        
Database.database().reference().child("Worker").child(userTaskFirebaseChild).ref.child("coord").observe(.childAdded, with: { (snapshot) in
            self.coordinatesWorker.append((snapshot.value) as! String)
        }, withCancel: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: (self.coordinatesWorker.first?.toDouble())! , longitude:(self.coordinatesWorker.last?.toDouble())!)
            let span = MKCoordinateSpan(latitudeDelta: 0.0015, longitudeDelta: 0.0015)
            let region = MKCoordinateRegion(center: location, span: span)
            self.mapView.setRegion(region, animated: true)
            self.annotation.coordinate = location
            self.annotation.title = "MY LAWYER"
            self.annotation.subtitle = "current location"
            self.mapView.addAnnotation(self.annotation)
            
        }
        
    }
    
    
    
    
    
}



extension TasksVC:  UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tasksTableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! TaskTableViewCell
        
        cell.taskLabelCell.text = userTasks[indexPath.row]
        return cell
    }
    
}
extension String
{
    /// EZSE: Converts String to Double
    public func toDouble() -> Double?
    {
        if let num = NumberFormatter().number(from: self) {
            return num.doubleValue
        } else {
            return nil
        }
    }
}



